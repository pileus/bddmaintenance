$(document).ready(function() {
    loadAll();
    
});

function loadAll(){
	var load = $.get('PHP/selectAll.php',function(oData){
					var liMarque, ulTypes, liType, ulModeles, liModele, ulMachines, li, ali;
					var divMachine,divModele,divType,divMarque;
					var oUl = document.getElementById('ulSearch');
					oUl.innerHTML='';
					var Marques = oData.getElementsByTagName("Marque");
					for (var i = 0 ; i < Marques.length ; i++) {
						liMarque = document.createElement("li");
						var ID_marque = Marques[i].getAttribute('ID');
						liMarque.innerHTML="<h4 search=\"?Marque="+ID_marque+"\">"+Marques[i].getAttribute("li")+"</h4>";
						ulTypes = document.createElement("ul");
						Types = Marques[i].getElementsByTagName("Type");
						for (var ii=0 ; ii<Types.length ; ii++){
							liType = document.createElement("li");
							liType.innerHTML="<h4 name=\"Type\" ID_type="+Types[ii].getAttribute('ID')+"><small>"+Types[ii].getAttribute("li")+"</small></h4>";
							ulModeles = document.createElement("ul");
							Modeles = Types[ii].getElementsByTagName("Modele");
							for (var iii=0 ; iii<Modeles.length ; iii++){
								ID_modele=Modeles[iii].getAttribute('ID');
								liModele = document.createElement("li");
								liModele.innerHTML="<h5 search=\"?Marque="+ID_marque+"&Modele="+ID_modele+"\">"+Modeles[iii].getAttribute("li")+"</h5>";
								ulMachines = document.createElement("ul");
								Machines = Modeles[iii].getElementsByTagName("Machine");
								for (var j=0 ; j<Machines.length ; j++){
									li = document.createElement("li");
									li.innerHTML="<h5></h5>";
									ali=Machines[j].getAttribute("codeBarre")+" - "+Machines[j].getAttribute("nomUsuel")+" - "+Machines[j].getAttribute("numSerie");
									divMachine = document.createElement('small');
									divMachine.setAttribute('search','?Marque='+ID_marque+"&Modele="+ID_modele+"&Machine="+Machines[j].getAttribute("ID"));
									divMachine.textContent=(ali);
									li.appendChild(divMachine);
									ulMachines.appendChild(li);
								}
								ulModeles.appendChild(liModele);
								ulModeles.appendChild(ulMachines);
							}
							ulTypes.appendChild(liType);
							ulTypes.appendChild(ulModeles);
						}
						oUl.appendChild(liMarque);
						oUl.appendChild(ulTypes);
					}
				},'xml');
	load.done(function(){
		$('#search').hideseek({
			hidden_mode: true,
			highlight: true,
			ignore_accents: true
		});

		$('[search]').css( 'cursor', 'pointer' );

		$('[search]').click(function(){
			if(window.location.pathname.split('/')[1].split('.')[1]=='php'){
				window.location.href = window.location.origin+'/index.php'+this.getAttribute('search');
			}
			else{
				window.location.href = window.location.origin+'/'+window.location.pathname.split('/')[1]+'/index.php'+this.getAttribute('search');
			}
		});
	});
}
