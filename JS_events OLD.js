
$(document).ready(function(){
	
	//Lecture de l'URL
	chargeMarques();
	var tables = ['Marque','Modele','Machine'];
	for(var table of tables){
		sessionVar(table,GETvar(table));
	}

	//Evénements sur select Marque
	$('#Marque').chosen().on("change",function(){
		hide(['divModeles','divFiles','divMachine','divComs']);
		set_disabled(['btnNewMachine']);
		remove_disabled(['btnNewModele']);
		sessionVar('Marque',$('#Marque').chosen().val());
		if($('#Marque').chosen().val()!=null){
			spanText('Marque');
			chargeModeles();
		}
	});

	//Evénements sur le select Modele
	$("#Modele").chosen().on("change",function (){
		hide(['divFiles','divMachine','divComs']);
		set_disabled(["btnNewMachine","btnEditModele"]);
		remove_disabled(['btnNewModele']);
		sessionVar('Modele',$("#Modele").chosen().val());
		if($('#Modele').chosen().val()!=null){
			show(['divFiles','divMachine']);
			remove_disabled(['btnEditModele','btnNewMachine']);
			chargeFichiers(document.getElementById("Liens"));
			chargeMachines();
			spanText('Modele');
			chargePannesModeles();
		}
	});

	//Evénements sur le select Machine
	$('#Machine').chosen().on("change",function (){
		hide(['divComs']);
		set_disabled(['btnEditMachine']);
		sessionVar('Machine',$('#Machine').chosen().val());
		if($('#Machine').chosen().val()!=null){
			chargeInterventions();
			chargeFicheMachine($('#Machine').chosen().val());
			show(['divComs']);
			remove_disabled(['btnEditMachine']);
		}
	});


	$('#btnChangeComs').on("click",function (){
		var xhr = new XMLHttpRequest();
		xhr.open("POST", "PHP/updateCommentaires.php",false);
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xhr.send("ID_machine="+ $('#Machine').chosen().val + "&Commentaires="+ $('#Commentaires').val());
		alert('Les commentaires ont été changés.');
	});

	$('#btnEditModele').on("click",function(){
		$('#modalEditModele').on('shown.bs.modal', function () {
        	chargeTypes();
			editModele();
        	$('.chosen-select', this).chosen('destroy').chosen();
        	$('#selectType').trigger('chosen:updated');
			$('#selectType').next().css( "width", "100%" );
    	});		
	});

});