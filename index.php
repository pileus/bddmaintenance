<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="icon" type="image/png" href="favicon.png" />
		<!-- Latest compiled and minified CSS -->
		<!-- <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"> -->
		<link rel="stylesheet" href="plugins/bootstrap-3.1.1-dist/css/bootstrap-chosen.css">

		<link rel="stylesheet" href="plugins/fileInput.css">

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    	<!-- <script type="text/javascript" src="plugins/jquery-1.11.0.js"></script> -->
    	
    	<!-- Latest compiled and minified JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
		<!-- <script type="text/javascript" src="plugins/bootstrap-3.1.1-dist/js/bootstrap.js"></script> -->

		<!-- <script src="http://harvesthq.github.io/chosen/chosen.jquery.js"></script> -->
		<script src="plugins/chosen_v1.4.2/chosen.jquery.min.js"></script>
		
		<title> BdD maintenance </title>
	</head>

	<body style="padding-bottom:70px">
		<h1 id="titre" class="text-center"> BdD Maintenance </h1>
			<?php 
				session_start();
				if (isset($_SESSION['Techno'])||$_SESSION['Techno']!=NULL){
					include("formMaintenance.php");
				}
				else {
					header('location:login.php?'.$_SERVER['QUERY_STRING']);
				}
				
			?>
	</body>

	<script>
	$(function() {
	  $('.chosen-select').chosen();
	});
	$("#btnNewMarque").show();
	$("#btnNewModele").show();
	$("#btnNewMachine").show();
	$("#btnNewType").show();
    </script>
</html>