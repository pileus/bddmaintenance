<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="icon" type="image/png" href="favicon.png" />
		<!-- Latest compiled and minified CSS -->
		<!-- <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"> -->
		<link rel="stylesheet" href="plugins/bootstrap-3.1.1-dist/css/bootstrap-chosen.css">

		<link rel="stylesheet" href="plugins/fileInput.css">

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    	<!-- <script type="text/javascript" src="plugins/jquery-1.11.0.js"></script> -->
    	
    	<!-- Latest compiled and minified JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
		<!-- <script type="text/javascript" src="plugins/bootstrap-3.1.1-dist/js/bootstrap.js"></script> -->

		<!-- <script src="http://harvesthq.github.io/chosen/chosen.jquery.js"></script> -->
		<script src="plugins/chosen_v1.4.2/chosen.jquery.min.js"></script>
		
		
		<script>
		    $(function() {
		      $('.chosen-select').chosen();
		    });
	    </script>


		
		<title> BdD maintenance </title>
	</head>

	<body style="background-color:#A0A0A0">
		<h1 id="titre" class="text-center"> BdD Maintenance </h1>
	<form method="post" action="PHP/sessionTechno.php">
		<div class="row">
			<div class="col-md-4">
			</div>
			<div class="col-md-4">
			<select name="Login" id="Logins" data-placeholder="Mais qui es-tu ?" class="chosen-select" onchange="selectTechno(this)" style="background-color:#C0C0C0" autofocus>
				<script type="text/javascript"> 
					$.get("PHP/selectLogins.php",
						function readLogins(oData){
							var nodes = oData.getElementsByTagName("item");
							var oSelect = document.getElementById("Logins");
							var oOption, oInner;
							oSelect.innerHTML = "";
							oOption = document.createElement("option");
							oOption.value = "";
							oSelect.appendChild(oOption);
							for (var i=0, c=nodes.length; i<c; i++) {
								oOption = document.createElement("option");
								oOption.value = nodes[i].getAttribute("id");
								oInner  = document.createTextNode(nodes[i].getAttribute("login"));
								oOption.appendChild(oInner);
								oSelect.appendChild(oOption);
							}
							oSelect.appendChild(oOption);
							if(navigator.appName!='Microsoft Internet Explorer'){
								$('#Logins').trigger("chosen:updated");
								$('#Logins').trigger("chosen:activate");
							}
						},"xml");

					function selectTechno(othis){
						if (othis.value != 0){
							document.getElementById("valider").removeAttribute("disabled");
						}
						else{
							document.getElementById("valider").setAttribute("disabled","disabled");
						}
					}
					if(navigator.appName=='Microsoft Internet Explorer'){
						$('h1').text('');
						$('h1').append('*** Ne fonctionne pas avec ce navigateur. ***');
					}
				</script>
			</select>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-4">
			</div>
			<div class="col-md-4">
				<button type="submit" id="valider" class="btn btn-lg btn-primary center-block" disabled="disabled">Valider..</button>
			</div>
		</div>

	</form>
	<script type="text/javascript">
		$(document).ready(function(){
			$('form').get(0).setAttribute('action',"PHP/sessionTechno.php"+window.location.search);
		});
	</script>
	</body>
</html>