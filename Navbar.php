<!- Navbar ->
<nav class="navbar navbar-default navbar-fixed-bottom">
	<div class="container-fluid">
    	<div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	    </div>

	    <div class="collapse navbar-collapse" id="navbar-collapse-1">
	    	<ul class="nav navbar-nav">
	        	<li><a href="PHP/session_destroy.php">Accueil</a></li>
	        	<li><a href="index.php">Par Marque</a></li>
	        	<li><a href="recherche.php">Recherche</a></li>
			</ul>
			<div class="navbar-right">
				<!-- Button trigger ModalNewMarque -->
				<button type="button" class="btn btn-primary navbar-btn" data-toggle="modal" data-target="#ModalNewMarque" id="btnNewMarque" >Nouvelle marque</button>
				<!-- Button trigger ModalNewModele -->
				<button type="button" class="btn btn-primary navbar-btn" data-toggle="modal" data-target="#ModalNewModele" id="btnNewModele" disabled="disabled">Nouveau modèle</button>
				<!-- Button trigger ModalNewModele -->
				<button type="button" class="btn btn-primary navbar-btn" data-toggle="modal" data-target="#ModalNewMachine" id="btnNewMachine" disabled="disabled">Nouvelle machine</button>
			</div>
		</div>

	 </div><!-- /.container-fluid -->
</nav>
<script type="text/javascript">
	$('#btnNewMarque').hide();
	$('#btnNewModele').hide();
	$('#btnNewMachine').hide();
	$('#btnNewType').hide();
</script>