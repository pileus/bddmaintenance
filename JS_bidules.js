function spanText(selectID){
	var lgth = document.getElementsByName('span'+selectID).length;
	var span = document.getElementsByName('span'+selectID);
	for (i=0 ; i<lgth ; i++){
		span[i].textContent = $('#'+selectID+' option:selected').text();
	}
}

function sessionVar(variable,ID){
	return $.post("PHP/sessionVar.php",{'var':variable , 'ID':ID},
		function(retour){
			var url=window.location;
			history.pushState("","",url.origin+url.pathname+retour);
			for(var i=0;i<$('form').length;i++){
				var urlAction = $('form').get(i).getAttribute('action')+window.location.search;
				$('form').get(i).setAttribute('action',urlAction);
			};
		});
}


function GETvar(variable){
	var parts = window.location.search.substr(1).split("&");
	var $_GET = {};
	for (var i = 0; i < parts.length; i++) {
	    var temp = parts[i].split("=");
	    $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
	}
	return($_GET[variable]);
}

function hide(IDs){
	for (var i = IDs.length - 1; i >= 0; i--) {
		$('#'+IDs[i]).hide();
	}
}

function show(IDs){
	for (var i = IDs.length - 1; i >= 0; i--) {
		$('#'+IDs[i]).show();
	}
}

function set_disabled(IDs){
	for (var i = 0 ; i<IDs.length ; i++) {
		document.getElementById(IDs[i]).setAttribute("disabled","disabled");
	}
}

function remove_disabled(IDs){
	for (var i = 0 ; i<IDs.length ; i++) {
		document.getElementById(IDs[i]).removeAttribute("disabled");
	}
}

function getURLparam(param){
	var o = window.location.search.substr(1).split('&');
	var params = new Object();
	for (var i of o){
		var data = i.split('=');
		params[data[0]]=data[1];
	}
	return params[param];
}