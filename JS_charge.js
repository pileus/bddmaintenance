//Pour les marques
function chargeMarques(){
	var load =	$.post("PHP/selectMarques.php",
					function (oData){
					var nodes = oData.getElementsByTagName("item");
					var oSelect = document.getElementById("Marque");
					var oOption, oInner;
					oSelect.innerHTML = "";
					for (var i=0, c=nodes.length; i<c; i++) {
						oOption = document.createElement("option");
						oOption.value = nodes[i].getAttribute("id");
						oInner  = document.createTextNode(nodes[i].getAttribute("name"));
						oOption.appendChild(oInner);
						oSelect.appendChild(oOption);
					}
					oSelect.appendChild(oOption);
					},
				"xml");
	load.done(function(){
				if(getURLparam('Marque')!=null){
					$('#Marque').chosen().val(getURLparam('Marque'));
					$('#Marque').trigger("chosen:updated").trigger("change");
				}
				else{
					$('#Marque').trigger("chosen:updated");
					$('#Marque').trigger('chosen:activate');
				}
				
			});
}


//Pour les modèles
function chargeModeles(){
	var load = $.post("PHP/selectModeles.php",
					function(oData){
						var parent = oData.getElementsByTagName("optgroup");
						var oOption, oInner, oGroup;
						var oSelect = document.getElementById("Modele");
						oSelect.innerHTML = "";
						oOption = document.createElement("option");
						oOption.value = "";
						oSelect.appendChild(oOption);
						for (var p=0, pp=parent.length; p<pp; p++) {
							oGroup = document.createElement("optgroup");
							oGroup.label = parent[p].getAttribute("label");
							var nodes = parent[p].getElementsByTagName("item");
							for (var i=0, c=nodes.length; i<c; i++) {
								oOption = document.createElement("option");
								oInner  = document.createTextNode(nodes[i].getAttribute("name"));
								oOption.value = nodes[i].getAttribute("id");
								oOption.appendChild(oInner);
								oGroup.appendChild(oOption);
							}
							oSelect.appendChild(oGroup);
						}
					},"xml");
	load.done(function(){
		if(getURLparam('Modele')!=null){
			$('#Modele').chosen().val(getURLparam('Modele'));
			$('#Modele').trigger("chosen:updated").trigger("change");
		}
		else{
			$('#Modele').trigger("chosen:updated");
			$('#Modele').trigger('chosen:activate');
		}
		$('#divModeles').show();
		$('#Modele').trigger('chosen:updated');
		$("#Modele").next().css( "width", "100%" );
		$('#Modele').trigger('activate');
	});
}

function editModele(){
	var load = $.post("PHP/selectModele.php",
		function (oXML){
			var item = oXML.getElementsByTagName("item");
			var oform = document.forms["formEditModele"];
			$(oform.elements["selectType"]).val(item[0].getAttribute("type")).trigger("chosen:updated");
			oform.elements["modele"].value=item[0].getAttribute("modele");
			oform.elements["nomUsuel"].value=item[0].getAttribute("nomUsuel");
		},"xml");
}

function selModele(txtCntnt){
	Modele = document.getElementById("Modele");
	for(i=0 ; i<Modele.getElementsByTagName("option").length ; i++){
		if (Modele.options[i].textContent == txtCntnt){
			Modele.selectedIndex = i;
			break;
		}
	}
}

function chargePannesModeles(){
	$.post('PHP/selectPannesModeles.php',function(oData){
		var idPannes = document.getElementById("idPannesModeles");
		idPannes.innerHTML = "";
		var pannes = oData.getElementsByTagName("panne"), actions;
		var panelPannes, panelPanne, panelHeading, panelTitle, aPanelTitle;
		var collapsePanne, panelBody, ulActions, liAction, divBtn;
		panelPannes=document.createElement("div");
		panelPannes.className="panel-group";
		panelPannes.setAttribute("id","collapseGroupPannesModeles");

		for (var i=0 ; i < pannes.length ; i++) {
			var idPanne = pannes[i].getAttribute("id");
			var date = pannes[i].getAttribute("date");
			var techno = pannes[i].getAttribute("techno");
			var txtPanne = pannes[i].firstChild.textContent;

			panelPanne=document.createElement("div");
			if (pannes[i].getAttribute("enCours") == true){
				panelPanne.className="panel panel-danger";
			}	
			else {
				panelPanne.className="panel panel-default";
			}
			panelHeading=document.createElement("div");
			panelHeading.className="panel-heading";
			panelTitle=document.createElement("h5");
			panelTitle.className="panel-title";
			aPanelTitle=document.createElement("a");
			aPanelTitle.setAttribute("data-toggle","collapse");
			aPanelTitle.setAttribute("data-parent","#collapseGroupPannesModeles");
			aPanelTitle.setAttribute("href","#collapseModele"+i);
			if (pannes[i].getAttribute("enCours") == true){
				var bouton = "<strong>"+date+" par "+techno+" : </strong><em>"+txtPanne+"</em>";
				bouton=bouton+'<button class="btn btn-danger btn-xs pull-right" onclick="document.getElementById(\'ID_panneModele\').value='+idPanne+'" data-toggle="modal" data-target="#ModalNewActionModele">Intervenir</button><div id="idActionModele'+i+'" hidden>';
			//	bouton = bouton+'<button class="btn btn-default btn-xs pull-right" onclick="fermePanneModele('+pannes[i].getAttribute("id")+')">Clôturer la panne</button>';
				aPanelTitle.innerHTML = bouton;
			}
			else{
				aPanelTitle.innerHTML = "<strong>"+date+" par "+techno+" : </strong><em>"+txtPanne+"</em>";
			}
				
			panelTitle.appendChild(aPanelTitle);
			panelHeading.appendChild(panelTitle);
			
			collapsePanne = document.createElement("div");
			collapsePanne.className="panel-collapse collapse";
			collapsePanne.setAttribute("id","collapseModele"+i);
			panelBody = document.createElement("div");
			panelBody.className="panel-body";

			actions = pannes[i].getElementsByTagName("action");
			ulActions=document.createElement("ul");
			for(var ii=0 ; ii<actions.length ; ii++){
				liAction=document.createElement("li");
				var date = actions[ii].getAttribute("date");
				var techno = actions[ii].getAttribute("techno");
				var txtAction = actions[ii].textContent;
				liAction.innerHTML = "<strong>"+techno+" le "+date+" :</strong><br>"+txtAction;
				ulActions.appendChild(liAction);
			}
			panelBody.appendChild(ulActions);
			divBtn=document.createElement("div");
			if (pannes[i].getAttribute("enCours") == true){
				divBtn.innerHTML='<button class="btn btn-primary btn-xs pull-right" onclick="fermePanneModele('+pannes[i].getAttribute("id")+')">Clôturer la panne</button>';
			//	divBtn.innerHTML='<button class="btn btn-default btn-sm pull-right" onclick="document.getElementById(\'ID_panneModele\').value='+idPanne+'" data-toggle="modal" data-target="#ModalNewActionModele">Intervenir</button><div id="idActionModele'+i+'" hidden>';	
				panelBody.appendChild(divBtn);
			}

			collapsePanne.appendChild(panelBody);
			panelPanne.appendChild(panelHeading);
			panelPanne.appendChild(collapsePanne);
			panelPannes.appendChild(panelPanne);
		};
		idPannes.appendChild(panelPannes);
	},'xml');
	
}

function fermePanneModele(ID){
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "PHP/updatePanneModele.php", false);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.send("ID_panneModele=" + ID);
	chargePannesModeles();
}

function chargeTypes(){
	console.log("ok");
	var load = $.post("PHP/selectTypes.php",function(oData){
				var nodes = oData.getElementsByTagName("item");
				var oSelect = document.querySelectorAll("#selectType");
				var oOption, oInner;
				for(var i=0, n=oSelect.length; i<n; i++){
					oSelect[i].innerHTML = "";
					for (var ii=0, c=nodes.length; ii<c; ii++) {
						oOption = document.createElement("option");
						oOption.value = nodes[ii].getAttribute("id");
						if(i==1 && ii==0){
							//oInner = document.createTextNode("Types déjà existants..");
							oInner = document.createTextNode("");
						}
						else {
							oInner  = document.createTextNode(nodes[ii].getAttribute("name"));
						}
						oOption.appendChild(oInner);
						oSelect[i].appendChild(oOption);
					}
				}
			},"xml");
	load.done(function(){
		$('#selectType').trigger('chosen:updated');
		$('#selectType').next().css( "width", "100%" );
		$('#selectType').trigger('activate');
	});
}


function chargeMachines(){
	var load = $.post("PHP/selectMachines.php",
					function(oData){
						var nodes = oData.getElementsByTagName("item");
						var oSelect = document.getElementById("Machine");
						
						var oOption, oInner;

						oSelect.innerHTML = "";
						oOption = document.createElement("option");
						oOption.value = "";
						oSelect.appendChild(oOption);

						for (var i=0, c=nodes.length; i<c; i++) {
							oOption = document.createElement("option");
							oOption.value = nodes[i].getAttribute("id");
							oInner  = document.createTextNode(nodes[i].getAttribute("name"));
							oOption.appendChild(oInner);
							oSelect.appendChild(oOption);
						}
					},"xml");
	load.done(function(){
		if(getURLparam('Machine')!=null){
			$('#Machine').chosen().val(getURLparam('Machine'));
			$('#Machine').trigger("chosen:updated").trigger("change");
		}
		else{
			$('#Machine').trigger("chosen:updated");
			$('#Machine').trigger('chosen:activate');
		}
		$('#divMachine').show();
		$('#Machine').trigger('chosen:updated');
		$('#Machine').next().css( "width", "100%" );
		$('#Machine').trigger('activate');
	});
}

function editMachine(){
	$.post("PHP/selectMachine.php",function(oXML){
		var item = oXML.getElementsByTagName("item");
		var oform = document.forms["formEditMachine"];
		// oform.elements["numSerie"].value=GETvar("numSerie");
		// oform.elements["codeBarre"].value=GETvar("codeBarre");
		// oform.elements["nomUsuel"].value=GETvar("nomUsuel");
		oform.elements["numSerie"].value=item[0].getAttribute("numSerie");
		oform.elements["codeBarre"].value=item[0].getAttribute("codeBarre");
		oform.elements["nomUsuel"].value=item[0].getAttribute("nomUsuel");
	},"xml");
}

function delMachine(){
	var ID_machine = $('#Machine').chosen().val();
	var ID_marque = $('#Marque').chosen().val();
	var ID_modele = $('#Modele').chosen().val();
	var xhr   = new XMLHttpRequest();
	if (confirm("Es-tu sûr de vouloir supprimer cette machine ?")==true) {
		if (confirm("Elle va vraiment être supprimer de la base de données.")==true) {
			xhr.open("POST", "PHP/delMachine.php", false);
			xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			xhr.send("ID_machine=" + ID_machine);
			alert("La machine a été supprimée.");
		}
	}
	var URL = window.location;
	var indexID = URL.href.indexOf("&Machine=");
	URL.replace(URL.href.substr(0,indexID),"");
}


function readCommentaires(oData) {
	var nodes = oData.getElementsByTagName("Commentaires");
	var oSelect = document.getElementById("Commentaires");
	var oInner;

	oSelect.innerHTML = "";
	oInner  = document.createTextNode(nodes[0].textContent);
	oSelect.appendChild(oInner);
}
function chargeCommentaires(IdM){
	var value = IdM.options[IdM.selectedIndex].value;
	
	var xhr   = new XMLHttpRequest();

	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			
			readCommentaires(xhr.responseXML);
			//document.getElementById("loader").style.display = "none";
		} else if (xhr.readyState < 4) {
			//document.getElementById("loader").style.display = "inline";
		}
	};

	xhr.open("POST", "PHP/selectCommentaires.php", false);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.send("ID_machine=" + value);
	document.getElementById('Coms').removeAttribute('hidden');

}


//Gestion des fichiers
function uploadFichier(){
	//Sélection de la balise HTML5 <progress>
	var progressBar = document.querySelector('#progressBar');

	//Open our formData Object
	//var form = new FormData(document.getElementById('formFichier'));
	var form = new FormData();

	//Create our XMLHttpRequest object
	var xhr = new XMLHttpRequest();

	//Open our connection usign the POST method
	xhr.open('POST','PHP/stockFichier.php',true);
	
	xhr.upload.onprogress = function(e) {
        progressBar.value = e.loaded;
        progressBar.max = e.total;
    };

	xhr.onload = function() { 
		alert('Upload terminé !!!');
		chargeFichiers(document.getElementById("Liens"));
		//document.querySelector('#fileInput').value='';
		progressBar.value=0; };

	//Append our file to the formData object
	form.append('monFichier',document.querySelector('#fileInput').files[0]);
	form.append('Marque',$('#Marque').chosen().val());
	form.append('Modele',$('#Modele').chosen().val());
	//Send the file
	xhr.send(form);
}

function chargeFichiers(Liens){
	Liens.innerHTML="";	
	$.post('PHP/selectFichiers.php',{Modele:$("#Modele").chosen().val()},function(oData){
		show('divFiles');
		var nodes = oData.getElementsByTagName("Fichier");
		for (var i=0, c=(nodes.length-2); i<c ; i++) {
			var trash,nomFichier ;
			var op = document.createElement("a");
			op.setAttribute('href',nodes[i].getAttribute("href"));
			op.setAttribute('target','blank');
			op.value = "";
			op.setAttribute('name',nodes[i].getAttribute("nom"));
			var h4 = document.createElement('strong');
			h4.innerHTML = nodes[i].getAttribute("nom")+"     ";
			op.appendChild(h4);
			$('#Liens').append(op);
			trash = document.createElement('span');
			trash.addEventListener("click", function(){delFichier(this.previousSibling.name),false});
			trash.className="glyphicon glyphicon-trash";
			$('#Liens').append(trash);
			$('#Liens').append(document.createElement("br"));
		}
	},'xml');

}

function delFichier(othis){
	if (confirm("Es-tu sûr de vouloir supprimer ce fichier ?")==true) {
		$.post('PHP/delFichier.php',{file:othis});
		chargeFichiers(document.getElementById("Liens"));
	}
}
/*function delFichier(othis){
	if (confirm("Es-tu sûr de vouloir supprimer ce fichier ?")==true) {
		var xhr = new XMLHttpRequest();
		xhr.open('POST','PHP/delFichier.php',false);
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xhr.send("file="+othis);
		chargeFichiers(document.getElementById("Liens"));
	}
}*/


function affichePath(chemin){
	var oSelect = document.getElementById("Liens");
	var delP = oSelect.querySelector('p');
	if(delP != null){
		delP.parentNode.removeChild(delP);
	}
	var eP = document.createElement('p');
	eP.className="bg-info text-right";
	eP.setAttribute('id','infoPath');
	eP.appendChild(document.createTextNode(chemin.value));
	var upBtn = document.createElement('input');
	upBtn.type="button";
	upBtn.className="btn btn-danger btn-xs";
	upBtn.value="Télécharger ce fichier";
	upBtn.setAttribute("onclick","uploadFichier()");
	eP.appendChild(upBtn);
	oSelect.appendChild(eP);
	//<input type="button" class="btn btn-default btn-xs" value="Uploader le fichier" onclick="uploadFichier()">
	delP = oSelect.querySelector('p');
	if (chemin.value==""){
		delP.parentNode.removeChild(delP);
	}
}

function chargeFicheMachine(value){	
	$.post('PHP/ficheMachine.php',function(oData){
		var node = oData.getElementById("Commentaires");
		var oSelect = document.getElementById("Commentaires");
		var oInner;

		oSelect.innerHTML = "";
		oInner  = document.createTextNode(node.textContent);
		oSelect.appendChild(oInner);

		var legend = document.getElementById('legendComs');
		var IdM = document.getElementById("Machine");
		var barCode = IdM.options[IdM.selectedIndex].textContent;
		var numSerie = oData.getElementById("numSerie").textContent;
		var date = oData.getElementById("dateCreation").textContent;
		legend.innerHTML='<span class="glyphicon glyphicon-barcode"></span> '+barCode+' <span class="glyphicon glyphicon-barcode"></span> N° de série:'+numSerie+' créée le:'+date;
	},'xml');
}

function recAction(idPanne,txti){
	var Id=document.getElementById('Machine');
	var Action = document.getElementById(txti).value;
	document.getElementById(txti).value = "";
	
	var post = $.post('PHP/recAction.php',{'ID_panne':idPanne , 'Action':Action});
	post.done(function(){
		alert("Nouvelle action sur la machine, enregistrée.");
		chargeInterventions();
	})
}

// function recAction(idPanne,txti){
// 	var Id=document.getElementById('Machine');
// 	var Action = document.getElementById(txti).value;
// 	document.getElementById(txti).value = "";

// 	var xhr = new XMLHttpRequest();
// 	xhr.open("POST", "PHP/recAction.php",false);
// 	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
// 	xhr.send("ID_panne="+idPanne+"&Action="+Action);
// 	chargeInterventions();
// }
function setAction(io,idActioni){
	if (io){
		document.getElementById(idActioni).removeAttribute("hidden");
	}
	else{
		document.getElementById(idActioni).setAttribute("hidden","true");
		var txt = document.getElementById(idActioni).getElementsByTagName("textarea");
		txt[0].value = "";
	}
}

function chargeInterventions(){
	$.post('PHP/selectInterventions.php',function(oData){
		var idPannes = document.getElementById("idPannes");
		idPannes.innerHTML = "";
		var pannes = oData.getElementsByTagName("panne"), actions;
		var panelPannes, panelPanne, panelHeading, panelTitle, aPanelTitle;
		var collapsePanne, panelBody, ulActions, liAction, divBtn;
		panelPannes=document.createElement("div");
		panelPannes.className="panel-group";
		panelPannes.setAttribute("id","collapseGroupPannes");

		for (var i=0 ; i < pannes.length ; i++) {
			var idPanne = pannes[i].getAttribute("id");
			var date = pannes[i].getAttribute("date");
			var techno = pannes[i].getAttribute("techno");
			var txtPanne = pannes[i].firstChild.textContent;

			panelPanne=document.createElement("div");
			if (pannes[i].getAttribute("enCours") == true){
				panelPanne.className="panel panel-danger";
			}	
			else {
				panelPanne.className="panel panel-default";
			}
			panelHeading=document.createElement("div");
			panelHeading.className="panel-heading";
			panelTitle=document.createElement("h5");
			panelTitle.className="panel-title";
			aPanelTitle=document.createElement("a");
			aPanelTitle.setAttribute("data-toggle","collapse");
			aPanelTitle.setAttribute("data-parent","#collapseGroupPannes");
			aPanelTitle.setAttribute("href","#collapse"+i);
			if (pannes[i].getAttribute("enCours") == true){
				var bouton = "<strong>"+date+" par "+techno+" : </strong><em>"+txtPanne+"</em>";
				bouton = bouton+'<button class="btn btn-danger btn-xs pull-right" onclick="setAction(true,\'idAction'+i+'\')">Intervenir</button><div id="idAction'+i+'" hidden><textarea class="form-control" id="txt'+i+'" name="Action" rows="3"> </textarea><button class="btn btn-danger btn-xs" onclick="recAction('+pannes[i].getAttribute("id")+',\'txt'+i+'\')">Enregistrer l\'intervention</button><button class="btn btn-primary btn-xs" onclick="setAction(false,\'idAction'+i+'\')"/>Annuler</button></div>';
				aPanelTitle.innerHTML = bouton;
			}
			else{
				aPanelTitle.innerHTML = "<strong>"+date+" par "+techno+" : </strong><em>"+txtPanne+"</em>";
			}
				
			panelTitle.appendChild(aPanelTitle);
			panelHeading.appendChild(panelTitle);
			
			collapsePanne = document.createElement("div");
			collapsePanne.className="panel-collapse collapse";
			collapsePanne.setAttribute("id","collapse"+i);
			panelBody = document.createElement("div");
			panelBody.className="panel-body";

			actions = pannes[i].getElementsByTagName("action");
			ulActions=document.createElement("ul");
			for(var ii=0 ; ii<actions.length ; ii++){
				liAction=document.createElement("li");
				var date = actions[ii].getAttribute("date");
				var techno = actions[ii].getAttribute("techno");
				var txtAction = actions[ii].textContent;
				liAction.innerHTML = "<strong>"+techno+" le "+date+" :</strong><br>"+txtAction;
				ulActions.appendChild(liAction);
			}
			panelBody.appendChild(ulActions);
			divBtn=document.createElement("div");
			if (pannes[i].getAttribute("enCours") == true){
				divBtn.innerHTML='<button class="btn btn-primary btn-xs pull-right" onclick="fermePanne('+pannes[i].getAttribute("id")+')">Clôturer la panne</button>';
				//divBtn.innerHTML='<button class="btn btn-default btn-sm pull-right" onclick="setAction(true,\'idAction'+i+'\')">Intervenir</button><div id="idAction'+i+'" hidden><textarea class="form-control" id="txt'+i+'" name="Action" rows="3"> </textarea><button class="btn btn-danger btn-xs" onclick="recAction('+pannes[i].getAttribute("id")+',\'txt'+i+'\')">Enregistrer l\'intervention</button><button class="btn btn-primary btn-xs" onclick="setAction(false,\'idAction'+i+'\')"/>Annuler</button></div>';
				panelBody.appendChild(divBtn);
			}
			collapsePanne.appendChild(panelBody);
			panelPanne.appendChild(panelHeading);
			panelPanne.appendChild(collapsePanne);
			panelPannes.appendChild(panelPanne);
		};
		idPannes.appendChild(panelPannes);
	},'xml');
	
}

function fermePanne(ID){
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "PHP/updatePanne.php", false);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.send("ID_panne=" + ID);
	chargeInterventions();
}