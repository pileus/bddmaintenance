<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="icon" type="image/png" href="favicon.png" />

		<link rel="stylesheet" href="plugins/fileInput.css">

    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    	
		<script src="plugins/jquery.hideseek.min.js"></script>

		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

		
		<title> BdD maintenance </title>
	</head>

	<body style="padding-bottom:70px">
		<h1 id="titre" class="text-center"> BdD Maintenance </h1>
			<?php 
				session_start();
				if (isset($_POST['Login'])){
					$_SESSION['Techno']=$_POST['Login'];
				}
				else if (isset($_SESSION['Techno'])==false) {
					header('location:index.php');
				}
			?>
		<!- Searchbar -><div class="row">
			<div class="col-md-3">
			</div>
			<div class="col-md-6">
				<input id="search" name="search" placeholder="Taper ici la recherche" type="text" data-list=".list" class="form-control" autofocus>	
					<ul id="ulSearch" class="list">
					  <li>Marque 1</li>
					  		<ul>
					  			<li>Modele 1.1.1</li>
							  		<ul>
							  			<li><a href="#">Machine 1.1.1.1</a></li>
							  		</ul>
							 </ul>
					  <li>Marque 2</li>
					  <ul>
					  		<li>Types 2.1</li>
							  		<ul>
							  			<li><a href="#">Machine 2.1.1</a></li>
							  		</ul>
					  	</ul>
					</ul>
			</div>
		</div>

		<!- Navbar ->
		<?php include("Navbar.php") ?>
		<script type="text/javascript">
			$('#btnNewMarque').hide();
			$('#btnNewModele').hide();
			$('#btnNewMachine').hide();
			$('#btnNewType').hide();
		</script>
		<script src="JS_selectAll.js"></script>
	</body>
</html>