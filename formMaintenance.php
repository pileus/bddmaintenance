<script type="text/javascript" src="JS_charge.js"></script>
<script type="text/javascript" src="JS_events.js"></script>
<script type="text/javascript" src="JS_bidules.js"></script>



<!- Liste des marques -><div class="row">
	<div class="col-md-4">
	</div>
	<div class="col-md-4">
	<select name="Marque" id="Marque" data-placeholder="Sélectionner une marque.." class="chosen-select" >
	</select>
	</div>
</div>


<!- Liste des modèles -><div id="divModeles" style="display:none">
	<div class="row">
		<label class="col-md-offset-4" for="Modele"><h4> Modèles disponibles pour <span name="spanMarque"></span> :</h4></label>
	</div>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<select name="Modele" id="Modele" data-placeholder="Sélectionner un modèle.." class="chosen-select">
			</select>
		</div>
		<div class="col-md-1">
			<button class="btn btn-warning btn-md" data-toggle="modal" data-target="#modalEditModele" id="btnEditModele" title="Modifier le modèle" disabled="disabled"><span class="glyphicon glyphicon-edit"></span></button>
		</div>
	</div>
</div>


<!- Fichiers associés et pannes sur le modèle ->
<div id="divFiles" style="display:none">
	<form method="post" name="formFichier">
	<div class="col-sm-offset-2 col-lg-8" style="padding-top:10px">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title"><strong>Fichiers pour les <span name="spanModele"></span></strong>
					<div class="pull-right hidden-xs">
						<span class="btn btn-default btn-file btn-xs">Choisir un fichier (100Mo max)<input type="file" id="fileInput" name="monFichier" onchange="affichePath(this)" onclick="this.value=''"></span>
						<progress id="progressBar" value=0 style="vertical-align:middle"></progress>
					</div>
				</div>
			</div>
			<div class="panel-body">
				<div id="Liens"></div>
			</div>
		</div>
	</div>
	</form>

	<div class="col-sm-offset-2 col-lg-8" style="padding-top:10px">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title"><strong>Pannes des <span name="spanModele"></span> :</strong>
					<button class="btn btn-warning btn-xs pull-right" data-toggle="modal" data-target="#ModalNewPanneModele">Enregistrer une nouvelle remarque</button>
				</div>
			</div>
			<div class="panel-body" id="idPannesModeles">
			</div>
		</div>
	</div>
</div>


<!- Liste des machines -><div id="divMachine" style="display:none">
	<div class="row">
		<label class="col-md-offset-4" for="Machine" ><h4> Machines existantes : </h4></label>
	</div>
	<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<select name="Machine" id="Machine" class="chosen-select" >
		</select>
	</div>
	<div class="col-md-1">
		<button class="btn btn-warning btn-md" onclick="editMachine()" data-toggle="modal" data-target="#ModalEditMachine" id="btnEditMachine" title="Modifier la machine sélectionnée" disabled="disabled"><span class="glyphicon glyphicon-edit"></span></button>
	</div>
	</div>
</div>


<!- Commentaires & Interventions ->
<div class="row" id="divComs" style="padding-top:10px ; display:none">
	<div class="col-md-offset-2 col-lg-8">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title"><strong id="legendComs"></strong>
					<button class="btn btn-danger btn-xs pull-right" onclick="delMachine()"><span class="glyphicon glyphicon-floppy-remove" title="Supprimer la machine de la base de données"></span></button>
					<button id="btnChangeComs" class="btn btn-info btn-xs pull-right">Changer les commentaires</button>
				</div>
			</div>
			<div class="panel-body">
				<textarea name="Commentaires" id="Commentaires" class="form-control" rows="3"> </textarea>		
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title"><strong>Interventions sur la machine :</strong>
					<button class="btn btn-warning btn-xs pull-right" data-toggle="modal" data-target="#ModalNewPanne">Enregistrer une nouvelle remarque</button>
				</div>
			</div>
			<div class="panel-body" id="idPannes"></div>
		</div>
	</div>
</div>

<!- Navbar ->
<?php include("Navbar.php") ?>

<!- Modals ->
<?php include("modals.php") ?>


</script>