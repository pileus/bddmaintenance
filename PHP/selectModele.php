<?php
	include ("connectBdD.php");
	header("Content-Type: text/xml"."\n");
	$XML = new DOMDocument('1.0','utf-8');
	$XML->formatOutput = true;

	$ID_modele=$_SESSION['Modele'];
	$query = $bdd->query("SELECT * FROM t_modeles WHERE ID_modele=".$ID_modele);
	$Modele = $query->fetch();

	$eModele=$XML->createElement('Modele');

	$eItem=$XML->createElement('item');
	$eItem->setAttribute("id",$Modele['ID_modele']);
	$eItem->setAttribute("type",$Modele['ID_type']);
	$eItem->setAttribute("modele",html_entity_decode($Modele['Modele']));
	$eItem->setAttribute("nomUsuel",html_entity_decode($Modele['nomUsuel']));
	
	$eModele->appendChild($eItem);
	
	$XML->appendChild($eModele);
	echo $XML->saveXML();

?>