<?php
	include ("connectBdD.php");

	header("Content-Type: text/xml"."\n");
	$XML = new DOMDocument('1.0','utf-8');
	$XML->formatOutput = true ;
	
	$eMachines = $XML->createElement('Machines');
	
//	$eMachines->appendChild(item("O"," - - - - - ",$XML));

	$ID_modele = $_SESSION['Modele'];
	$query = $bdd->query("SELECT * FROM t_machines WHERE ID_modele=".$ID_modele." ORDER BY codeBarre");
	$bdd=null;
	

	while ($data=$query->fetch()) {
		$name = $data['codeBarre']." - ".html_entity_decode($data['nomUsuel']);
		$eMachines->appendChild(item($data['ID_machine'],$name,$XML));
		//echo '<item id="'.$data['ID_machine'].'" name="'.$data['codeBarre'].'" />';
	}
	

	function item($id,$name,$XML){
		$eMachine=$XML->createElement('item');
		$eMachine->setAttribute('id',$id);
		$eMachine->setAttribute('name',$name);
		return $eMachine;
	}

	$XML->appendChild($eMachines);
	echo $XML->saveXML();
?>