<?php
	include('connectBdD.php');
	$bdd=null;
	$Marque = $_SESSION['Marque'];
	$Modele = $_POST['Modele'];	
	
	header("Content-Type: text/xml");
	$XML = new DOMDocument('1.0','utf-8');
	$XML->formatOutput = true;
	
	$eFichiers=$XML->createElement('Fichiers');
	
	$dir = $Marque."/".$Modele;
	if(is_dir(getcwd().'/'.$dir)){
		$dir = getcwd().'/'.$dir;
		if(scandir($dir,1)!=false){
			$b = scandir($dir,1);
			// Sort in ascending order - this is default
			//$a = scandir($dir);
		}
		else {$b=false;}
	}
	else {$dir=false; $b=false;}
	
	if ($b != false){
		foreach ($b as $key => $value) {
			if ($value != '@eaDir'){ 	//Juste pour échapper un dossier caché lié au Synology
			$eFichier=$XML->createElement('Fichier');
			$eFichier->setAttribute("id",$key);
			$eFichier->setAttribute("href",'Docs/'.$Marque.'/'.$Modele.'/'.$value);
			$eFichier->setAttribute("nom",$value);
			$eFichiers->appendChild($eFichier);
			}
		}
	}

	$XML->appendChild($eFichiers);
	echo $XML->saveXML();
?>