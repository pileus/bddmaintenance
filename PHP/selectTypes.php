<?php

	include ("connectBdD.php");
		
	header("Content-Type: text/xml"."\n");
	$XML = new DOMDocument('1.0','utf-8');
	$XML->formatOutput = true;

	$query = $bdd->query("SELECT * FROM t_types ORDER BY Type");
	$bdd = null;
	
	$eList=$XML->createElement('list');

	$eList->appendChild(item("","",$XML));

	while ($data=$query->fetch()) {
		$Type = html_entity_decode($data['Type']);
		$eList->appendChild(item($data['ID_type'],$Type,$XML));
	}

	function item($id,$name,$XML){
		$eType=$XML->createElement('item');
		$eType->setAttribute('id',$id);
		$eType->setAttribute('name',$name);
		return $eType;
	}

	$XML->appendChild($eList);
	echo $XML->saveXML();

?>