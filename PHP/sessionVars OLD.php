<?php
	include ("connectBdD.php");
	$_SESSION['url']='';
	if (isset($_SESSION['Marque']) && $_SESSION['Marque']!=null) {
		$_SESSION['url']="?Marque=".$_SESSION['Marque'];
		echo $_SESSION['url'];
		$query = $bdd->query("SELECT ID_modele FROM t_modeles WHERE ID_marque=".$_SESSION['Marque']);
		$Modeles = $query->fetchAll(PDO::FETCH_COLUMN);
		if (in_array($_SESSION['Modele'], $Modeles)) {
			$_SESSION['url']=$_SESSION['url']."&Modele=".$_SESSION['Modele'];
			echo "&Modele=".$_SESSION['Modele'];
			$query = $bdd->query("SELECT ID_machine FROM t_machines WHERE ID_modele=".$_SESSION['Modele']);
			$Machines = $query->fetchAll(PDO::FETCH_COLUMN);
			if (in_array($_SESSION['Machine'], $Machines)) {
				$_SESSION['url']=$_SESSION['url']."&Machine=".$_SESSION['Machine'];
				echo "&Machine=".$_SESSION['Machine'];
			}
			else{
				unset($_SESSION['Machine']);
			}
		}
		else{
			unset($_SESSION['Modele']);
			unset($_SESSION['Machine']);
		}
	}
	else{
		unset($_SESSION['Marque']);
		unset($_SESSION['Modele']);
		unset($_SESSION['Machine']);
	};
$bdd=null;
?>