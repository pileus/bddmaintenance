<?php

	include ("connectBdD.php");
	header("Content-Type: text/xml"."\n");
	$XML = new DOMDocument('1.0','utf-8');
	$XML->formatOutput = true;

	$query = $bdd->query(
		"SELECT 
			t_marques.Marque,t_marques.ID_marque,t_types.Type,t_types.ID_type,t_modeles.ID_modele,t_modeles.Modele,
			ID_machine,t_machines.codeBarre,t_machines.nomUsuel,t_machines.numSerie
		FROM t_marques
		LEFT JOIN t_modeles 	ON t_marques.ID_marque=t_modeles.ID_marque
		INNER JOIN t_types		ON t_modeles.ID_type=t_types.ID_type
		LEFT JOIN t_machines	ON t_modeles.ID_modele=t_machines.ID_modele
		ORDER BY Marque, Type, Modele, codeBarre
		"
	);
	while ($data=$query->fetch(PDO::FETCH_ASSOC)) {
		$Machines[]=$data;
	}

	$eMachines=$XML->createElement('Machines');

	for ($i=0;$i<count($Machines);$i++) {
		$e=$Machines[$i];
		if ($i==0) {
			$Marque=$XML->createElement('Marque');
			$Marque->setAttribute('li',$e['Marque']);
			$Marque->setAttribute('ID',$e['ID_marque']);
			$Type=$XML->createElement('Type');
			$Type->setAttribute('li',$e['Type']);
			$Type->setAttribute('ID',$e['ID_type']);
			$Modele=$XML->createElement('Modele');
			$Modele->setAttribute('li',$e['Modele']);
			$Modele->setAttribute('ID',$e['ID_modele']);
		}

		if($e['ID_machine']!=NULL){
			$Machine=$XML->createElement('Machine');
			$Machine->setAttribute('ID',$e['ID_machine']);
			$Machine->setAttribute('codeBarre',$e['codeBarre']);
			$Machine->setAttribute('nomUsuel',$e['nomUsuel']);
			$Machine->setAttribute('numSerie',$e['numSerie']);
			$Modele->appendChild($Machine);
		}

		if ($Machines[$i+1]['ID_modele']!=$e['ID_modele']) {
			$Type->appendChild($Modele);
		}
		if($Machines[$i+1]['Type']!=$e['Type']){
			$Marque->appendChild($Type);
			if($Machines[$i+1]['Marque']!=$e['Marque']){
				$eMachines->appendChild($Marque);
				if($i<(count($Machines)-1)){
					$Marque=$XML->createElement('Marque');
					$Marque->setAttribute('li',$Machines[$i+1]['Marque']);
					$Marque->setAttribute('ID',$Machines[$i+1]['ID_marque']);
				}
			}
			if($i<(count($Machines)-1)){
				$Type=$XML->createElement('Type');
				$Type->setAttribute('li',$Machines[$i+1]['Type']);
				$Type->setAttribute('ID',$Machines[$i+1]['ID_type']);
			}
		}
		if($i<(count($Machines)-1)){
			$Modele=$XML->createElement('Modele');
			$Modele->setAttribute('li',$Machines[$i+1]['Modele']);
			$Modele->setAttribute('ID',$Machines[$i+1]['ID_modele']);
		}
	}
	$XML->appendChild($eMachines);
	echo $XML->saveXML();
?>