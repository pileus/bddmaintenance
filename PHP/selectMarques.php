<?php
	include ("connectBdD.php");
		
	header("Content-Type: text/xml"."\n");
	$XML = new DOMDocument('1.0','utf-8');
	$XML->formatOutput = true;

	$query = $bdd->query("SELECT * FROM t_marques ORDER BY Marque");
	$bdd = null;
	
	$eMarques=$XML->createElement('Marques');

	//$eMarques->appendChild(item("0","Liste des marques..",$XML));
	$eMarques->appendChild(item("","",$XML));

	while ($data=$query->fetch()) {
		$Marque = html_entity_decode($data['Marque']);
		$eMarques->appendChild(item($data['ID_marque'],$Marque,$XML));
	}

	function item($id,$name,$XML){
		$eMarque=$XML->createElement('item');
		$eMarque->setAttribute('id',$id);
		$eMarque->setAttribute('name',$name);
		return $eMarque;
	}

	$XML->appendChild($eMarques);
	echo $XML->saveXML();

?>