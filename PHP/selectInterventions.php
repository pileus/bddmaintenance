<?php
	include ("connectBdD.php");
	
	header("Content-Type: text/xml"."\n");
	$XML = new DOMDocument('1.0','utf-8');
	$XML->formatOutput = true;
	
	$ID_machine = $_SESSION['Machine'];

	$ePannes = $XML->createElement('Pannes');
	//$XML->appendChild($ePannes);

	$query = $bdd->query("SELECT * FROM t_pannes WHERE ID_machine=".$ID_machine." ORDER BY Date");
	
	while($data=$query->fetch()){
		$ePanne = $XML->createElement('panne');
		//$ePannes->appendChild($ePanne);

		//ID_panne
		$ePanne->setAttribute('id',$data['ID_panne']);
		//date
		$ePanne->setAttribute('date',date('d-m-Y', strtotime($data['Date'])));
		//techno
		$query2 = $bdd->query("SELECT surnom FROM t_technos WHERE ID_techno=".$data['ID_techno']) or exit(print_r($bdd->errorInfo()));
		$surnom = $query2->fetch();
		$ePanne->setAttribute('techno',$surnom['surnom']);
		//Etat en cours
		$ePanne->setAttribute('enCours',$data['enCours']);

		//Description de la panne
		$panne = $XML->createTextNode($data['Panne']);
		$ePanne->appendChild($panne);

		//Récupération des Interventions liées à la panne
		$query2 = $bdd->query("SELECT * FROM t_actions WHERE ID_panne=".$data['ID_panne']) or exit(print_r($bdd->errorInfo()));
		while($data2=$query2->fetch()){
			$eAction = $XML->createElement('action');
			//$eActions->appendChild($eAction);
			//ID_action
			$eAction->setAttribute('id',$data2['ID_action']);
			//date
			$eAction->setAttribute('date',date('d-m-Y', strtotime($data2['Date'])));
			//techno
			$query3 = $bdd->query("SELECT surnom FROM t_technos WHERE ID_techno=".$data2['ID_techno']) or exit(print_r($bdd->errorInfo()));
			$surnom = $query3->fetch();
			$eAction->setAttribute('techno',$surnom['surnom']);

			//Description de l'action
			$action = $XML->createTextNode($data2['Action']);
			$eAction->appendChild($action);

			$ePanne->appendChild($eAction);
		}
		$ePannes->appendChild($ePanne);
	}
	$XML->appendChild($ePannes);
	echo $XML->saveXML();
?>