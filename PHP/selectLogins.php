<?php
	include ("connectBdD.php");
		
	header("Content-Type: text/xml"."\n");
	$XML = new DOMDocument('1.0','utf-8');
	$XML->formatOutput = true;

	$query = $bdd->query("SELECT * FROM t_technos ORDER BY surnom");
	$bdd = null;
	
	$eLogins=$XML->createElement('Logins');

	//$eLogins->appendChild(item("0","Choisir un nom de technicien..",$XML));

	while ($data=$query->fetch()) {
		$Login = html_entity_decode($data['surnom']);
		$eLogins->appendChild(item($data['ID_techno'],$Login,$XML));
	}

	function item($id,$name,$XML){
		$eLogin=$XML->createElement('item');
		$eLogin->setAttribute('id',$id);
		$eLogin->setAttribute('login',$name);
		return $eLogin;
	}

	$XML->appendChild($eLogins);
	echo $XML->saveXML();

?>