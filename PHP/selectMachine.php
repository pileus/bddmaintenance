<?php
	include ("connectBdD.php");

	header("Content-Type: text/xml"."\n");
	$XML = new DOMDocument('1.0','utf-8');
	$XML->formatOutput = true ;
	
	$eMachines = $XML->createElement('Machine');
	
	$query = $bdd->query('SELECT * FROM t_machines where ID_machine='.$_SESSION['Machine']);
	$m = $query->fetch();

	$eMachines->appendChild(item($m['numSerie'],$m['codeBarre'],$m['nomUsuel'],$XML));

	function item($numSerie,$codeBarre,$nomUsuel,$XML){
		$eMachine=$XML->createElement('item');
		$eMachine->setAttribute('numSerie',html_entity_decode($numSerie));
		$eMachine->setAttribute('codeBarre',$codeBarre);
		$eMachine->setAttribute('nomUsuel',html_entity_decode($nomUsuel));
		return $eMachine;
	}

	$XML->appendChild($eMachines);
	echo $XML->saveXML();

	$bdd=null;
?>