<?php
	include ("connectBdD.php");

	header("Content-Type: text/xml");
	echo '<?xml version="1.0" encoding="utf-8"?>';	

	$ID_machine = $_SESSION['Machine'];
	
	$query = $bdd->query("SELECT * FROM t_machines WHERE ID_machine=".$ID_machine);
	$data=$query->fetch();
	
	echo "<ficheMachine>\n";

	$str=html_entity_decode($data['Date']);
	$str=date('d-m-Y', strtotime($str));
	//echo date($str,'d-m-Y')."\n";
	echo '<dateCreation id="dateCreation">'.$str."</dateCreation>\n";

	$str=html_entity_decode($_SESSION['numSerie']);
	echo '<numSerie id="numSerie">'.$str."</numSerie>\n";

	$str=html_entity_decode($data['Commentaires']);
	echo('<Commentaires id="Commentaires">'.$str.'');
	echo "</Commentaires>\n";

	echo "</ficheMachine>";

	$bdd=NULL;

?>