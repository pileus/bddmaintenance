<?php
	include ("connectBdD.php");
	header("Content-Type: text/xml"."\n");
	$XML = new DOMDocument('1.0','utf-8');
	$XML->formatOutput = true;

	$ID_marque=$_SESSION['Marque'];
	$query = $bdd->query("SELECT ID_type,ID_modele,ID_marque,Modele FROM t_modeles WHERE ID_marque=".$ID_marque." ORDER BY ID_type");
	$tablo = $query->fetchAll(PDO::FETCH_ASSOC|PDO::FETCH_GROUP);

	$eModeles=$XML->createElement('Modeles');

	foreach ($tablo as $ID_type => $optgroup) {
		$query = $bdd->query("SELECT Type FROM t_types WHERE ID_type=".$ID_type);
		$type = $query->fetch();
		$eGroup=$XML->createElement('optgroup');
		$eGroup->setAttribute('label',html_entity_decode($type['Type']));
		foreach($optgroup as $option) {
			$eItem=$XML->createElement('item');
			$eItem->setAttribute("id",$option['ID_modele']);
			$eItem->setAttribute("name",html_entity_decode($option['Modele']));
			$eGroup->appendChild($eItem);
		}
		$eModeles->appendChild($eGroup);
	}
	
	$XML->appendChild($eModeles);
	echo $XML->saveXML();

?>