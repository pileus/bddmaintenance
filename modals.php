<!- Modal Nouvelle Marque ->
<div class="modal fade" id="ModalNewMarque" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
		<div class="modal-body">
			<form class="form-horizontal" method="post" action="PHP/recMarque.php">
				<input type="text" name="newMarque" class="form-control" placeholder="Entrer ici le nom de la marque à créer." required>
				<button type="submit" class="btn btn-danger btn-sm">Enregistrer la nouvelle marque.. <i class="glyphicon glyphicon-ok-circle"></i></button>
			</form>
		</div><!-- /.modal-body -->
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
		</div><!-- /.modal-footer -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog-->
</div><!-- /.modal -->

<!- Modal Nouveau modèle ->
<div class="modal fade" id="ModalNewModele" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title">Entrer un nouveau modèle pour la marque <span name="spanMarque">xxx</span>.</h4>
		</div>		
		<div class="modal-body">
			<form class="form-horizontal" method="post" action="PHP/recModele.php" name="formModel">
				<select name="selectType" id="selectType" class="form-control input-lg col-md-2" autofocus required>
					<script type="text/javascript">chargeTypes()</script>
				</select>
				<!-- Button trigger ModalNewType -->
				<button type="button" class="btn" data-toggle="modal" data-target="#ModalNewType" id="btnNewType">Nouveau type</button>
				<input type="text" name="newModele" class="form-control input-lg" placeholder="Entrer ici le nom du modèle à créer." required>
				<input type="text" name="nomUsuel" class="form-control input-lg" placeholder="Nom usuel de la machine" />
				<button type="submit" class="btn btn-danger btn-sm">Enregistrer le nouveau modèle.. <i class="glyphicon glyphicon-ok-circle"></i></button>
			</form>
		</div><!-- /.modal-body -->
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
		</div><!-- /.modal-footer -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog-->
</div><!-- /.modal -->

<!- Modal Modifier modèle ->
<div class="modal fade" id="modalEditModele" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title">Modification du modèle <span name="spanModele">xxx</span> de la marque <span name="spanMarque">xxx</span>.</h4>
		</div>		
		<div class="modal-body">
			<form class="form-horizontal" method="post" action="PHP/updateModele.php" name="formEditModele">
				<select name="selectType" id="selectType" data-placeholder="Sélectionner un type.." class="chosen-select" required>	</select>
				<input type="text" name="modele" class="form-control input-lg" placeholder="Entrer ici le nom du modèle à créer." required>
				<input type="text" name="nomUsuel" class="form-control input-lg" placeholder="Nom usuel de la machine" />
				<button type="submit" class="btn btn-danger btn-sm">Enregistrer les modifications.. <i class="glyphicon glyphicon-ok-circle"></i></button>
			</form>
		</div><!-- /.modal-body -->
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
		</div><!-- /.modal-footer -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog-->
</div><!-- /.modal -->

<!- Modal Nouvelle Panne Modele-><div class="modal fade" id="ModalNewPanneModele" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
		<div class="modal-body">
			<form name="formPanneModele" class="form-horizontal" method="post" action="PHP/recPanneModele.php">
				<textarea name="Panne" class="form-control" rows="3" required ></textarea>				
				<button type="submit" class="btn btn-danger btn-sm">Enregistrer la nouvelle remarque.. <i class="glyphicon glyphicon-ok-circle"></i></button>
			</form>
		</div><!-- /.modal-body -->
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
		</div><!-- /.modal-footer -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog-->
</div><!-- /.modal -->

<!-- Modal Nouvelle Action Modele--><div class="modal fade" id="ModalNewActionModele" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
		<div class="modal-body">
			<form name="formActionModele" class="form-horizontal" method="post" action="PHP/recActionModele.php">
				<textarea name="actionModele" class="form-control" rows="3" required ></textarea>				
				<input type="text" id="ID_panneModele" name="ID_panneModele" hidden />
				<button type="submit" class="btn btn-danger btn-sm">Enregistrer la nouvelle intervention.. <i class="glyphicon glyphicon-ok-circle"></i></button>
			</form>
		</div><!-- /.modal-body -->
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
		</div><!-- /.modal-footer -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog-->
</div><!-- /.modal -->

<!- Modal Nouvelle machine ->
<div class="modal fade" id="ModalNewMachine" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title">Création d'une machine <span name="spanMarque">xxx</span> <span name="spanModele">xxx</span>.</h4>
		</div>		
		<div class="modal-body">
			<form class="form-horizontal" method="post" action="PHP/recMachine.php" name="formMachine">
				<input type="text" name="numSerie" class="form-control input-lg" placeholder="Numéro de série de la machine" required />
				<input type="text" name="codeBarre" class="form-control input-lg" placeholder="Code barre de la machine" />
				<input type="text" name="nomUsuel" class="form-control input-lg" placeholder="Nom usuel de la machine" />
				<textarea name="Commentaires" class="form-control input-lg" placeholder="Écrire ici d'éventuels commentaires sur la machine.." rows="3"></textarea>
				<button type="submit" class="btn btn-danger btn-sm">Enregistrer la nouvelle machine.. <i class="glyphicon glyphicon-ok-circle"></i></button>
			</form>
		</div><!-- /.modal-body -->
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
		</div><!-- /.modal-footer -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog-->
</div><!-- /.modal -->

<!- Modal modifier machine ->
<div class="modal fade" id="ModalEditMachine" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title">Modification d'une machine <span name="spanMarque">xxx</span> <span name="spanModele">xxx</span>.</h4>
		</div>		
		<div class="modal-body">
			<form class="form-horizontal" method="post" action="PHP/updateMachine.php" name="formEditMachine">
				<label>N° de série</label>
				<input type="text" name="numSerie" class="form-control input-lg" placeholder="Numéro de série de la machine" required />
				<label>Code barre</label>
				<input type="text" name="codeBarre" class="form-control input-lg" placeholder="Code barre de la machine" required />
				<label>Nom usuel</label>
				<input type="text" name="nomUsuel" class="form-control input-lg" placeholder="Nom usuel de la machine" />
				<button type="submit" class="btn btn-danger btn-sm">Enregistrer les modifications.. <i class="glyphicon glyphicon-ok-circle"></i></button>
			</form>
		</div><!-- /.modal-body -->
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
		</div><!-- /.modal-footer -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog-->
</div><!-- /.modal -->

<!-- Modal Nouvelle Panne Machine --><div class="modal fade" id="ModalNewPanne" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
		<div class="modal-body">
			<form name="formPanne" class="form-horizontal" method="post" action="PHP/recPanne.php">
				<textarea name="Panne" class="form-control" rows="3" required ></textarea>				
				<button type="submit" class="btn btn-danger btn-sm">Enregistrer la nouvelle remarque.. <i class="glyphicon glyphicon-ok-circle"></i></button>
			</form>
		</div><!-- /.modal-body -->
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
		</div><!-- /.modal-footer -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog-->
</div><!-- /.modal -->

<!- Modal Nouveau Type -><div class="modal fade" id="ModalNewType" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
		<div class="modal-body">
			<form name="formType" class="form-horizontal" method="post" action="PHP/recType.php">
				<input type="text" name="Type" class="form-control input-lg" placeholder="Le type à créer.." required />
				<button type="submit" class="btn btn-danger btn-sm">Enregistrer le nouveau type.. <i class="glyphicon glyphicon-ok-circle"></i></button>
			</form>
		</div><!-- /.modal-body -->
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Annuler</button>
		</div><!-- /.modal-footer -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog-->
</div><!-- /.modal -->

